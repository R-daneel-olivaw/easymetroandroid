package com.tw.akshat.easymetro;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

//Immutable

/**
 * This class is used to represent a metro stations as nodes in the undirected weighted graph used
 * to model the metro station network.
 * <p/>
 * The class is intended to be immutable and is marked final to prevent dilution of its
 * immutability by a subclass.
 * <p/>
 * The class does not expose any public constructors, instead uses a static factory methods for
 * better control of the instantiation of the class. The class uses a static object cache for
 * avoiding instantiation of objects where possible.
 *
 * @author Akshat
 */
public final class MetroStation {

    // name of the metro station
    private String stationName;

    // the different lines that this station is a participates in
    private String[] stationLines;

    // the static cache used to store the objects of this class
    private static Map<String, MetroStation> stationCache = new HashMap<String, MetroStation>();

    /**
     * A static factory method used for creating or retrieving objects of this class.
     *
     * @param stationName the name of the station
     * @param stationLines the lines the station is part of
     * @return an object representing the metro station, either a new object is created or one is
     * returned from the cache.
     *
     * @throws IllegalArgumentException If the provided parameters are invalid
     * @throws IllegalArgumentException If a object with the same name but different details already
     * exists in the cache
     */
    public static MetroStation createMetroStation(String stationName,
                                                  String... stationLines) {

        if (stationName == null || stationLines == null || stationLines.length == 0) {
            throw new IllegalArgumentException("illegal metro station details");
        }

        MetroStation cachedStation = stationCache.get(stationName.toLowerCase());
        if (cachedStation == null) {
            cachedStation = new MetroStation(stationName, stationLines);
            stationCache.put(stationName.toLowerCase(), cachedStation);
            return cachedStation;
        } else if (Arrays.deepEquals(cachedStation.getStationLines(), stationLines)) {
            return cachedStation;
        } else {
            throw new IllegalArgumentException("station with same exists with diffrent details");
        }
    }

    /**
     * A utility method that is used to retrieve an object of the type from the object cache.
     * @param stationName the nam of the station that is to be retrieved
     * @return the object with the same name, null if not found in cache
     *
     */
    public static MetroStation getStation(String stationName) {
        return stationCache.get(stationName.toLowerCase());
    }

    private MetroStation(String stationName, String... stationLines) {
        this.stationName = stationName;
        this.stationLines = stationLines;
    }

    public String getStationName() {
        return stationName;
    }

    public String[] getStationLines() {
        return stationLines;
    }

    @Override
    public String toString() {
        return this.stationName;
    }

    // Dont need to override equals and hashcode as we are now using an object cache.
/*	@Override
    public boolean equals(Object obj) {
		if (!(obj instanceof MetroStation)) {
			return false;
		}

		if (this == obj) {
			return true;
		}

		MetroStation m2 = (MetroStation) obj;
		if (m2.getStationName().equalsIgnoreCase(this.getStationName())
				&& Arrays.deepEquals(m2.getStationLines(),
						this.getStationLines())) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {

		return this.getStationName().hashCode()
				+ Arrays.deepHashCode(this.getStationLines());
	}*/
}
