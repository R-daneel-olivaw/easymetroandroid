package com.tw.akshat.easymetro;

import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.tw.akshat.easymetro.util.MetroRouteSearchResult;

import java.util.List;

/**
 * This activity is used to display the results of the search m
 */
public class RouteDetailActivity extends AppCompatActivity {

    // the results that beed to be displayed by the activity
    private MetroRouteSearchResult route;

    private RecyclerView recyclerView;
    private TextView duration;
    private TextView cost;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_route_detail);

        // retrieve the object representing the search result details of which need to be displayed by the activity
        Parcelable parcelableRoute = getIntent().getParcelableExtra("route");
        if (parcelableRoute instanceof MetroRouteSearchResult) {
            route = (MetroRouteSearchResult) parcelableRoute;
        } else {
            throw new IllegalArgumentException("Illegal route");
        }

        // capture and initialize UI components
        recyclerView = (RecyclerView) findViewById(R.id.rvRoute);
        duration = (TextView) findViewById(R.id.duration_mins);
        cost = (TextView) findViewById(R.id.cost);

        recyclerView.setAdapter(new RouteAdapter(route.getRoute()));
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        duration.setText(getString(R.string.string_duration) + route.getDurationMins() + getString(R.string.string_mins));
        cost.setText(getString(R.string.string_cost) + " : $" + route.getCost());
    }

    /**
     * The class is used as an adapter for the Recycler view used in the activity
     */
    private class RouteAdapter extends RecyclerView.Adapter<RouteViewHolder> {

        // the list stations that define the route
        private List<MetroStation> stations;

        private RouteAdapter(List<MetroStation> stations) {
            this.stations = stations;
        }

        @Override
        public RouteViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_item, parent, false);
            return new RouteViewHolder(v);
        }

        @Override
        public void onBindViewHolder(RouteViewHolder holder, int position) {

            MetroStation station = stations.get(position);
            holder.getMetaInfo().setText("");

            // display the name of the station
            holder.getStationName().setText(station.getStationName());

            final String[] lines = station.getStationLines();
            // check if the station is a junction i.e. has more than one lines passing through it
            if (lines.length == 1) {

                // if its not a junction then we use the color of the only line for the station
                holder.getFirstHalf().setBackgroundColor(getResources().getColor(getColorResourceCode(lines[0])));
                holder.getSecondHalf().setBackgroundColor(getResources().getColor(getColorResourceCode(lines[0])));
            } else {
                // if the station is a junction, we consider the previous and the next station on the route
                MetroStation prev = null;
                MetroStation next = null;
                // handle the case if the previous or the next station are null
                if (position != 0) {
                    prev = stations.get(position - 1);
                }
                if (position != stations.size() - 1) {
                    next = stations.get(position + 1);
                }

                if (null == prev) {
                    // if the previous station is null, then we use the color of the next station
                    holder.getFirstHalf().setBackgroundColor(getResources().getColor(getColorResourceCode(next.getStationLines()[0])));
                    holder.getSecondHalf().setBackgroundColor(getResources().getColor(getColorResourceCode(next.getStationLines()[0])));
                } else if (null == next) {
                    // if the next station is null, then we use the color of the previous station
                    holder.getFirstHalf().setBackgroundColor(getResources().getColor(getColorResourceCode(prev.getStationLines()[0])));
                    holder.getSecondHalf().setBackgroundColor(getResources().getColor(getColorResourceCode(prev.getStationLines()[0])));
                } else {
                    // if both previous and next are not null then we use both the colors
                    String prevLine = prev.getStationLines()[0];
                    String nextLine = next.getStationLines()[0];
                    holder.getFirstHalf().setBackgroundColor(getResources().getColor(getColorResourceCode(prevLine)));
                    holder.getSecondHalf().setBackgroundColor(getResources().getColor(getColorResourceCode(nextLine)));

                    // update UI for indicating change
                    if (!prevLine.equalsIgnoreCase(nextLine)) {
                        holder.getMetaInfo().setText(R.string.string_change);
                    }
                }
            }

            if (position == 0) {
                holder.getMetaInfo().setText(R.string.string_start);
            } else if (position == stations.size() - 1) {
                holder.getMetaInfo().setText(R.string.string_end);
            }
        }

        /**
         * this method is used to fetch colors for each line.
         *
         * @param line identifier
         * @return the color resource ID
         */
        private int getColorResourceCode(String line) {

            if (line.equalsIgnoreCase("red")) {
                return R.color.colorRed;
            } else if (line.equalsIgnoreCase("green")) {
                return R.color.colorGreen;
            } else if (line.equalsIgnoreCase("blue")) {
                return R.color.colorBlue;
            } else if (line.equalsIgnoreCase("yellow")) {
                return R.color.colorYellow;
            } else if (line.equalsIgnoreCase("black")) {
                return R.color.colorBlack;
            }

            return R.color.colorDefault;
        }

        @Override
        public int getItemCount() {
            return stations.size();
        }
    }

    /**
     * A class used for using the optimizations in the recycler view
     */
    private class RouteViewHolder extends RecyclerView.ViewHolder {
        private TextView stationName;
        private FrameLayout firstHalf;
        private FrameLayout secondHalf;
        private TextView metaInfo;

        public RouteViewHolder(View itemView) {
            super(itemView);

            stationName = (TextView) itemView.findViewById(R.id.station_name);
            firstHalf = (FrameLayout) itemView.findViewById(R.id.first_half);
            secondHalf = (FrameLayout) itemView.findViewById(R.id.second_half);
            metaInfo = (TextView) itemView.findViewById(R.id.meta_info);
        }

        public TextView getStationName() {
            return stationName;
        }

        public FrameLayout getFirstHalf() {
            return firstHalf;
        }

        public FrameLayout getSecondHalf() {
            return secondHalf;
        }

        public TextView getMetaInfo() {
            return metaInfo;
        }
    }
}
