package com.tw.akshat.easymetro.util;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

import com.tw.akshat.easymetro.MetroStation;

/**
 * This class is used to encapsulate the results of the route search. The objects of the class are
 * intended to b immutable. The class is marked final as it is not desirable to have a subclass
 * break the immutability. There are no public setter methods.
 *
 * The class implements the interface {@link Parcelable} for enabling the android framework to
 * store the state of the object in an parcel.
 *
 * @author Akshat
 */
public final class MetroRouteSearchResult implements Parcelable {

    // source metro station
    private MetroStation src;

    // destination metro station
    private MetroStation dest;

    // the list of metro station that for the route between the source and destination
    private List<MetroStation> route;

    // the cost of the route
    private int cost;

    // the duration of the journey
    private double durationMins;

    /**
     * The constructor used to create the objects of the type. All the arguments are
     * mandatory, the primitive arguments can hae zero value.
     *
     * @param src the source station
     * @param dest the destination station
     * @param route the route between the source and destination station
     * @param cost the cost of the route
     * @param durationMins the duration of the journey
     *
     * @throws IllegalArgumentException in case the arguments are invalid
     */
    public MetroRouteSearchResult (MetroStation src, MetroStation dest, List<MetroStation> route, int cost, double durationMins)
    {
        if (null == src || null == dest || null == route || route.size() == 0) {
            throw new IllegalArgumentException("invalid parameters");
        }

        this.src = src;
        this.dest = dest;
        this.route = route;
        this.cost = cost;
        this.durationMins = durationMins;
    }

    /**
     * This constructor is used for reanimating the object by using the state stored inside a parcel.
     *
     * @param in the
     */
    protected MetroRouteSearchResult(Parcel in) {

        // extract data from the parcel in the same order used during packing
        cost = in.readInt();
        durationMins = in.readDouble();
        src = MetroStation.getStation(in.readString());
        dest = MetroStation.getStation(in.readString());

        // convert the list of string into a list of MetroStation objects
        List<String> stationNames = new ArrayList<String>();
        in.readStringList(stationNames);
        route = new ArrayList<MetroStation>();
        for (String name:stationNames) {
            route.add(MetroStation.getStation(name));
        }
    }

    /**
     * this method is provided by the {@link Parcelable} interface and is used by the android
     * platform to save the state of the object into the parcel.
     *
     * @param dest  the parcel to be used to store the state of the object
     * @param flags additional flags about how the object should be written
     */
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        // the order in which the values are inserted in the parcel is important
        dest.writeInt(cost);
        dest.writeDouble(durationMins);
        dest.writeString(src.getStationName());
        dest.writeString(this.dest.getStationName());
        dest.writeStringList(getStringStationList());
    }

    /**
     * this method is used to convert the list of {@link MetroStation} objects into a list of String.
     * This is done for storing the state of the object.
     * @return a list of string that represents the list of {@link MetroStation} objects.
     */
    private List<String> getStringStationList() {

        List<String> stationNameList = new ArrayList<String>();
        for(MetroStation m : route)
        {
            stationNameList.add(m.getStationName());
        }

        return stationNameList;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    /**
     * This class is used by the android platform for the reanimation of {@link MetroStation}
     * objects from a parcel.
     */
    public static final Creator<MetroRouteSearchResult> CREATOR = new Creator<MetroRouteSearchResult>() {
        @Override
        public MetroRouteSearchResult createFromParcel(Parcel in) {
            return new MetroRouteSearchResult(in);
        }

        @Override
        public MetroRouteSearchResult[] newArray(int size) {
            return new MetroRouteSearchResult[size];
        }
    };

    public int getCost() {
        return cost;
    }

    public List<MetroStation> getRoute() {
        return route;
    }

    public double getDurationMins() {
        return durationMins;
    }

    public MetroStation getSrc() {
        return src;
    }

    public MetroStation getDest() {
        return dest;
    }
}
