package com.tw.akshat.easymetro;

import com.tw.akshat.easymetro.util.MetroRouteSearchResult;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.jgrapht.GraphPath;
import org.jgrapht.Graphs;
import org.jgrapht.WeightedGraph;
import org.jgrapht.alg.DijkstraShortestPath;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.SimpleWeightedGraph;

/**
 * This class forms the core of the search feature of the application. This class uses a undirected
 * weighted graph to represent the metro network. The class uses Dijkstra's shortest path algorithm
 * to find the route with the least weight. The weight of the edges is defined with respect to the
 * time it takes to travers the edge. Nodes of the graph are represented by objects of type
 * {@link MetroStation}.
 * <p/>
 * The visibility of default is intentional as the class is not intended to be exposed to other
 * packages.
 *
 * @author Akshat
 * @since 2016-03-14
 */
class EasyMetroCore {

    // The graph object used to model the metro network. Uses {@link MetroStation} for nodes and
    // weight edges for represent the time it takes to traverse the edge.
    private WeightedGraph<MetroStation, DefaultWeightedEdge> graph;

    /**
     * For constructing a new object.
     */
    EasyMetroCore() {
        // populate the graph with the nodes representative of the metro stations and the edges
        // representing the connections between them.
        graph = populateGraph();
    }

    /**
     * This method is the single interface method used for the search functionality. This method
     * computes the shortest path between two nodes by using Dijkstra's shortest path algorithm
     * over the undirected weighted graph. The result is encapsulated in objects of type
     * {@link MetroRouteSearchResult}.
     * <p/>
     * The weight of each edge is representational of the time it takes to traverse that edge. The
     * cost of each route is calculated using the number of stations involved in the route and the
     * number of interchanges in the journey.
     *
     * @param src  the source station name
     * @param dest the destination station name
     * @return
     */
    MetroRouteSearchResult computeRoute(String src, String dest) {
        // use the shortest path algorithm over the undirected weighted graph
        DijkstraShortestPath<MetroStation, DefaultWeightedEdge> shortestPathAlgo = new DijkstraShortestPath<MetroStation, DefaultWeightedEdge>(
                graph, MetroStation.getStation(src),
                MetroStation.getStation(dest));
        // get the object representing the shortest path between the source and the destination nodes
        GraphPath<MetroStation, DefaultWeightedEdge> path = shortestPathAlgo.getPath();
        List<MetroStation> vertexList = Graphs.getPathVertexList(path);

        int cost = calculateCost(vertexList);

        return new MetroRouteSearchResult(path.getStartVertex(), path.getEndVertex(), vertexList, cost, path.getWeight());
    }

    /**
     * The method is used to calculate the cost of a route. The calculation cost is based on the
     * number of stations involved in the trip and the number of line changes in the trip.
     *
     * @param vertexList the list of stations that comprise of the route.
     * @return the cost of the route.
     */
    private int calculateCost(List<MetroStation> vertexList) {

        Set<String> metroLineSet = new HashSet<String>();

        for (MetroStation station : vertexList) {

            String[] stationLines = station.getStationLines();
            if (stationLines.length == 1) {
                metroLineSet.add(stationLines[0]);
            }
        }

        // cost = ($1 per number of station) + ($1 per line change)
        int cost = 1 * (vertexList.size() - 1) + 1 * (metroLineSet.size() - 1);

        // handle if both start and end stations are the same
        if (cost < 0) {
            cost = 0;
        }

        return cost;
    }

    /**
     * This method is used to populate the graph object in accordance with the metro network.
     *
     * @return the populated undirected weighted graph, representing the metro network.
     */
    private WeightedGraph<MetroStation, DefaultWeightedEdge> populateGraph() {

        // this is not the ideal implementation as it is very inflexible and the metro network is
        // defined in code. A better implementation will use an intermediate representation like
        // json to materialize the graph.
        WeightedGraph<MetroStation, DefaultWeightedEdge> g = new SimpleWeightedGraph<MetroStation, DefaultWeightedEdge>(
                DefaultWeightedEdge.class);

        // the interchange stations are added to th graph first, thought they are unconnected.
        addInterchangeStations(g);

        // add individual lines one after the other. Each line adds all the nodes and edges between
        // the nodes.
        addBlueLine(g);
        addGreenLine(g);
        addRedLine(g);
        addYellowLine(g);
        addBlackLine(g);

        return g;
    }

    private void addInterchangeStations(
            WeightedGraph<MetroStation, DefaultWeightedEdge> g) {

        // create an object for each station of the line
        MetroStation cityCenter = MetroStation.createMetroStation(
                "City Centre", "blue", "green");
        MetroStation greenCross = MetroStation.createMetroStation(
                "Green Cross", "green", "yellow");
        MetroStation southPark = MetroStation.createMetroStation("South Park",
                "green", "black");
        MetroStation eastEnd = MetroStation.createMetroStation("East end",
                "blue", "black");
        MetroStation boxingAvenue = MetroStation.createMetroStation(
                "Boxing avenue", "blue", "red");
        MetroStation morpheusLane = MetroStation.createMetroStation(
                "Morpheus Lane", "red", "yellow");
        MetroStation neoLane = MetroStation.createMetroStation("Neo Lane",
                "red", "black");

        // add the nodes to the graph
        g.addVertex(cityCenter);
        g.addVertex(greenCross);
        g.addVertex(southPark);
        g.addVertex(eastEnd);
        g.addVertex(boxingAvenue);
        g.addVertex(morpheusLane);
        g.addVertex(neoLane);

        // no edges added as this method only adds the stations common between lines
    }

    private void addBlackLine(WeightedGraph<MetroStation, DefaultWeightedEdge> g) {

        // create an object for each station of the line;
        MetroStation gothamStreet = MetroStation.createMetroStation(
                "Gotham street", "black");
        MetroStation batmanStreet = MetroStation.createMetroStation(
                "Batman street", "black");
        MetroStation jokersStreet = MetroStation.createMetroStation(
                "Jokers street", "black");
        MetroStation hawkinsStreet = MetroStation.createMetroStation(
                "Hawkins street", "black");
        MetroStation daVinciLane = MetroStation.createMetroStation(
                "DaVinci lane", "black");
        MetroStation newtonBathTub = MetroStation.createMetroStation(
                "Newton bath tub", "black");
        MetroStation einsteinLane = MetroStation.createMetroStation(
                "Einstein lane", "black");

        // add the nodes to the graph
        g.addVertex(gothamStreet);
        g.addVertex(batmanStreet);
        g.addVertex(jokersStreet);
        g.addVertex(hawkinsStreet);
        g.addVertex(daVinciLane);
        g.addVertex(newtonBathTub);
        g.addVertex(einsteinLane);

        DefaultWeightedEdge weightedEdge = null;
        // add edges to represent a metro line in the graph
        weightedEdge = g.addEdge(MetroStation.getStation("East end"),
                gothamStreet);
        // assign a weight to the edge
        g.setEdgeWeight(weightedEdge, 5);

        weightedEdge = g.addEdge(gothamStreet, batmanStreet);
        g.setEdgeWeight(weightedEdge, 5);

        weightedEdge = g.addEdge(batmanStreet, jokersStreet);
        g.setEdgeWeight(weightedEdge, 5);

        weightedEdge = g.addEdge(jokersStreet, hawkinsStreet);
        g.setEdgeWeight(weightedEdge, 5);

        weightedEdge = g.addEdge(hawkinsStreet, daVinciLane);
        g.setEdgeWeight(weightedEdge, 5);

        weightedEdge = g.addEdge(daVinciLane, MetroStation.getStation("South Park"));
        g.setEdgeWeight(weightedEdge, 5);

        weightedEdge = g.addEdge(MetroStation.getStation("South Park"), newtonBathTub);
        g.setEdgeWeight(weightedEdge, 5);

        weightedEdge = g.addEdge(newtonBathTub, einsteinLane);
        g.setEdgeWeight(weightedEdge, 5);

        weightedEdge = g.addEdge(einsteinLane, MetroStation.getStation("Neo Lane"));
        g.setEdgeWeight(weightedEdge, 5);

    }

    private void addYellowLine(
            WeightedGraph<MetroStation, DefaultWeightedEdge> g) {

        MetroStation orangeStreet = MetroStation.createMetroStation(
                "Orange Street", "yellow");
        MetroStation silkBoard = MetroStation.createMetroStation("Silk Board",
                "yellow");
        MetroStation snakePark = MetroStation.createMetroStation("Snake Park",
                "yellow");
        MetroStation littleStreet = MetroStation.createMetroStation(
                "Little Street", "yellow");
        MetroStation cricketGrounds = MetroStation.createMetroStation(
                "Cricket Grounds", "yellow");

        g.addVertex(orangeStreet);
        g.addVertex(silkBoard);
        g.addVertex(snakePark);
        g.addVertex(littleStreet);
        g.addVertex(cricketGrounds);

        DefaultWeightedEdge weightedEdge = null;

        weightedEdge = g.addEdge(MetroStation.getStation("Green Cross"),
                orangeStreet);
        g.setEdgeWeight(weightedEdge, 5);

        weightedEdge = g.addEdge(orangeStreet, silkBoard);
        g.setEdgeWeight(weightedEdge, 5);

        weightedEdge = g.addEdge(silkBoard, snakePark);
        g.setEdgeWeight(weightedEdge, 5);

        weightedEdge = g.addEdge(snakePark,
                MetroStation.getStation("Morpheus Lane"));
        g.setEdgeWeight(weightedEdge, 5);

        weightedEdge = g.addEdge(MetroStation.getStation("Morpheus Lane"),
                littleStreet);
        g.setEdgeWeight(weightedEdge, 5);

        weightedEdge = g.addEdge(littleStreet, cricketGrounds);
        g.setEdgeWeight(weightedEdge, 5);
    }

    private void addRedLine(WeightedGraph<MetroStation, DefaultWeightedEdge> g) {

        MetroStation matrixStand = MetroStation.createMetroStation(
                "Matrix Stand", "red");
        MetroStation keymakersLane = MetroStation.createMetroStation(
                "Keymakers Lane", "red");
        MetroStation oracleLane = MetroStation.createMetroStation(
                "Oracle Lane", "red");
        MetroStation cypherLane = MetroStation.createMetroStation(
                "Cypher lane", "red");
        MetroStation smithLane = MetroStation.createMetroStation("Smith lane",
                "red");
        MetroStation trinityLane = MetroStation.createMetroStation(
                "Trinity lane", "red");

        g.addVertex(matrixStand);
        g.addVertex(keymakersLane);
        g.addVertex(oracleLane);
        g.addVertex(cypherLane);
        g.addVertex(smithLane);
        g.addVertex(trinityLane);

        DefaultWeightedEdge weightedEdge = null;

        weightedEdge = g.addEdge(matrixStand, keymakersLane);
        g.setEdgeWeight(weightedEdge, 5);

        weightedEdge = g.addEdge(keymakersLane, oracleLane);
        g.setEdgeWeight(weightedEdge, 5);

        weightedEdge = g.addEdge(oracleLane,
                MetroStation.getStation("Boxing avenue"));
        g.setEdgeWeight(weightedEdge, 5);

        weightedEdge = g.addEdge(MetroStation.getStation("Boxing avenue"),
                cypherLane);
        g.setEdgeWeight(weightedEdge, 5);

        weightedEdge = g.addEdge(cypherLane, smithLane);
        g.setEdgeWeight(weightedEdge, 5);

        weightedEdge = g.addEdge(smithLane, MetroStation.getStation("Morpheus Lane"));
        g.setEdgeWeight(weightedEdge, 5);

        weightedEdge = g.addEdge(MetroStation.getStation("Morpheus Lane"), trinityLane);
        g.setEdgeWeight(weightedEdge, 5);

        weightedEdge = g.addEdge(trinityLane, MetroStation.getStation("Neo Lane"));
        g.setEdgeWeight(weightedEdge, 5);

    }

    private void addGreenLine(WeightedGraph<MetroStation, DefaultWeightedEdge> g) {

        MetroStation northPole = MetroStation.createMetroStation("North Pole",
                "green");
        MetroStation sheldonStreet = MetroStation.createMetroStation(
                "Sheldon Street", "green");
        MetroStation greenland = MetroStation.createMetroStation("Greenland",
                "green");
        MetroStation stadiumHouse = MetroStation.createMetroStation(
                "Stadium House", "green");
        MetroStation greenHouse = MetroStation.createMetroStation(
                "Green House", "green");
        MetroStation southPole = MetroStation.createMetroStation("South Pole",
                "green");

        g.addVertex(northPole);
        g.addVertex(sheldonStreet);
        g.addVertex(greenland);
        g.addVertex(stadiumHouse);
        g.addVertex(greenHouse);
        g.addVertex(southPole);

        DefaultWeightedEdge weightedEdge = null;

        weightedEdge = g.addEdge(northPole, sheldonStreet);
        g.setEdgeWeight(weightedEdge, 5);

        weightedEdge = g.addEdge(sheldonStreet, greenland);
        g.setEdgeWeight(weightedEdge, 5);

        weightedEdge = g.addEdge(greenland,
                MetroStation.getStation("City Centre"));
        g.setEdgeWeight(weightedEdge, 5);

        weightedEdge = g.addEdge(MetroStation.getStation("City Centre"),
                stadiumHouse);
        g.setEdgeWeight(weightedEdge, 5);

        weightedEdge = g.addEdge(stadiumHouse, greenHouse);
        g.setEdgeWeight(weightedEdge, 5);

        weightedEdge = g.addEdge(greenHouse, MetroStation.getStation("Green Cross"));
        g.setEdgeWeight(weightedEdge, 5);

        weightedEdge = g.addEdge(MetroStation.getStation("Green Cross"), southPole);
        g.setEdgeWeight(weightedEdge, 5);

        weightedEdge = g.addEdge(southPole, MetroStation.getStation("South Park"));
        g.setEdgeWeight(weightedEdge, 5);

    }

    private void addBlueLine(WeightedGraph<MetroStation, DefaultWeightedEdge> g) {

        MetroStation footStand = MetroStation.createMetroStation("Foot stand",
                "blue");
        MetroStation footballStadium = MetroStation.createMetroStation(
                "Football stadium", "blue");
        MetroStation peterPark = MetroStation.createMetroStation("Peter Park",
                "blue");
        MetroStation maximus = MetroStation.createMetroStation("Maximus",
                "blue");
        MetroStation rockyStreet = MetroStation.createMetroStation(
                "Rocky Street", "blue");
        MetroStation boxersStreet = MetroStation.createMetroStation(
                "Boxers Street", "blue");
        MetroStation westEnd = MetroStation.createMetroStation("West End",
                "blue");

        g.addVertex(footStand);
        g.addVertex(footballStadium);
        g.addVertex(peterPark);
        g.addVertex(maximus);
        g.addVertex(rockyStreet);
        g.addVertex(boxersStreet);
        g.addVertex(westEnd);

        DefaultWeightedEdge weightedEdge = null;

        weightedEdge = g.addEdge(MetroStation.getStation("East end"), footStand);
        g.setEdgeWeight(weightedEdge, 5);

        weightedEdge = g.addEdge(footStand, footballStadium);
        g.setEdgeWeight(weightedEdge, 5);

        weightedEdge = g.addEdge(footballStadium, MetroStation.getStation("City Centre"));
        g.setEdgeWeight(weightedEdge, 5);

        weightedEdge = g.addEdge(MetroStation.getStation("City Centre"), peterPark);
        g.setEdgeWeight(weightedEdge, 5);

        weightedEdge = g.addEdge(peterPark, maximus);
        g.setEdgeWeight(weightedEdge, 5);

        weightedEdge = g.addEdge(maximus, rockyStreet);
        g.setEdgeWeight(weightedEdge, 5);

        weightedEdge = g.addEdge(rockyStreet, boxersStreet);
        g.setEdgeWeight(weightedEdge, 5);

        weightedEdge = g.addEdge(boxersStreet, MetroStation.getStation("Boxing avenue"));
        g.setEdgeWeight(weightedEdge, 5);

        weightedEdge = g.addEdge(MetroStation.getStation("Boxing avenue"), westEnd);
        g.setEdgeWeight(weightedEdge, 5);
    }
}
