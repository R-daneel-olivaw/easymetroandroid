package com.tw.akshat.easymetro;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tw.akshat.easymetro.util.MetroRouteSearchResult;

import java.util.Arrays;

/**
 * This is the main activity of the application. I provides the search features for the application.
 */
public class MainActivity extends AppCompatActivity {

    // UI components
    private AutoCompleteTextView srcField;
    private AutoCompleteTextView destField;
    private Button goButton;
    private TextView duration;
    private TextView cost;
    private TextView meta;
    private LinearLayout resultContainer;
    private Button showRouteButton;
    private RelativeLayout errorContainer;
    private TextView errorText;

    // the result of the route search
    private MetroRouteSearchResult metroRouteSearchResult;

    // the array of metro stations used for the autocomplete text view
    private String[] metroStations = {"Batman street", "Boxers Street", "Boxing avenue", "City Centre", "Cricket Grounds", "Cypher lane", "DaVinci lane", "Einstein lane", "Foot,stand", "Football stadium", "Gotham street", "Green Cross", "Green House", "Greenland", "Hawkins street", "Jokers street", "Keymakers Lane", "Little Street", "Matrix Stand", "Maximus", "Morpheus Lane", "Neo Lane", "Newton bath tub", "North Pole", "Oracle Lane", "Orange Street", "Peter Park", "Rocky Street", "Sheldon Street", "Silk Board", "Smith lane", "Snake Park", "South Park", "South Pole", "Stadium House", "Trinity lane", "West End", "East end"};

    // this object is used to drive the search features using Dijkstra's shortest path algorithm.
    private EasyMetroCore searchCore;

    // listener used for handling the  soft keyboard buttons for easier navigation in the landscape
    // orientation
    private TextView.OnEditorActionListener imeActionListner = new TextView.OnEditorActionListener() {
        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            if (actionId == EditorInfo.IME_ACTION_NEXT) {
                destField.requestFocus();
            } else if (actionId == EditorInfo.IME_ACTION_DONE) {
                prepareForSearch();
            }
            return true;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // set the layout used by he activity
        setContentView(R.layout.activity_main);

        // assign layout ui components
        srcField = (AutoCompleteTextView) findViewById(R.id.src_field);
        srcField.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                destField.requestFocus();
            }
        });
        srcField.setOnEditorActionListener(imeActionListner);

        destField = (AutoCompleteTextView) findViewById(R.id.dest_field);
        destField.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                goButton.requestFocus();
                closeKeyboard();
            }
        });
        destField.setOnEditorActionListener(imeActionListner);

        // set adapters for the auto complete text view
        ArrayAdapter<String> srcAdapter = new ArrayAdapter<String>
                (this, android.R.layout.select_dialog_item, metroStations);
        ArrayAdapter<String> destAdapter = new ArrayAdapter<String>
                (this, android.R.layout.select_dialog_item, metroStations);

        srcField.setAdapter(srcAdapter);
        destField.setAdapter(destAdapter);

        // set listeners for the search button
        goButton = (Button) findViewById(R.id.go_button);
        goButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                prepareForSearch();
            }
        });

        // capture UI components used to show the results of the search
        duration = (TextView) findViewById(R.id.duration_mins);
        cost = (TextView) findViewById(R.id.cost);
        meta = (TextView) findViewById(R.id.search_meta);
        resultContainer = (LinearLayout) findViewById(R.id.result_container);
        showRouteButton = (Button) findViewById(R.id.show_route);
        errorContainer = (RelativeLayout) findViewById(R.id.error_container);
        errorText = (TextView) findViewById(R.id.error);

        // handle the click for the button for showing the details of the route by launching a different activity
        showRouteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (metroRouteSearchResult == null) {
                    return;
                }
                Intent intent = new Intent(MainActivity.this, RouteDetailActivity.class);
                // pass the search results to the next activity
                intent.putExtra("route", metroRouteSearchResult);
                startActivity(intent);
            }
        });

        // init the search helper object
        searchCore = new EasyMetroCore();
    }


    /**
     * trigger the search of route between two stations
     *
     * @param src  source station name
     * @param dest destination station name
     */
    private void searchRoute(String src, String dest) {

        // perform the search on a worker thread
        new SearchTask().execute(src, dest);
    }

    /**
     * this method is used to check if the inputs are valid and if its safe to perform the actual search.
     */
    private void prepareForSearch() {

        // close the soft keyboard if open for UI responsiveness
        closeKeyboard();

        // hide results
        resultContainer.setVisibility(View.GONE);

        // hide errors
        errorContainer.setVisibility(View.GONE);

        // fetch text for input fields
        String src = srcField.getText().toString().trim();
        String dest = destField.getText().toString().trim();

        boolean isError = false;
        // check if the source field is empty or if its a valid name of metro station
        if (src == null || src.length() == 0 || !Arrays.asList(metroStations).contains(src)) {
            // error
            isError = true;

            String text = errorText.getText().toString();
            errorText.setText(R.string.string_invalid_input);
        }

        // check if the destination field is empty or if its a valid name of metro station
        if (dest == null || dest.length() == 0 || !Arrays.asList(metroStations).contains(dest)) {
            // error dest
            isError = true;
            errorText.setText(R.string.string_invalid_input);
        }

        // check if src and destination are the same
        if (!isError && src.equalsIgnoreCase(dest)) {
            isError = true;
            errorText.setText(R.string.string_same_src_dest);
        }

        // start search only if there were no errors
        if (!isError) {

            searchRoute(src, dest);
        } else {
            errorContainer.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.reset:
                resetSearch();
                break;

            default:
                return false;
        }

        return true;
    }

    /**
     * this method is used to  reset the fields of the search form
     */
    private void resetSearch() {
        // clear fields
        srcField.setText("");
        destField.setText("");

        // hide results
        resultContainer.setVisibility(View.GONE);
    }

    /**
     * this method is used to close the keyboard
     */
    private void closeKeyboard() {

        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(srcField.getWindowToken(), 0);
        goButton.requestFocus();
    }

    /**
     * this class is sed to execute the search task on a worker thread.
     */
    private class SearchTask extends AsyncTask<String, Void, MetroRouteSearchResult> {

        @Override
        protected MetroRouteSearchResult doInBackground(String... params) {

            MetroRouteSearchResult result = searchCore.computeRoute(params[0], params[1]);

            return result;
        }

        @Override
        protected void onPostExecute(MetroRouteSearchResult metroRouteSearchResult) {
            MainActivity.this.metroRouteSearchResult = metroRouteSearchResult;

            // update the UI when the results of the search are returned
            duration.setText(getString(R.string.string_duration) + metroRouteSearchResult.getDurationMins() + getString(R.string.string_mins));
            cost.setText(getString(R.string.string_cost) + " : $" + metroRouteSearchResult.getCost());
            meta.setText(getString(R.string.string_from) + ":" + metroRouteSearchResult.getSrc().getStationName() + ", " + getString(R.string.string_to) + ":" + metroRouteSearchResult.getDest().getStationName());

            resultContainer.setVisibility(View.VISIBLE);
        }
    }
}
